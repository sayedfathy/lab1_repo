from openerp.osv import fields, osv
from mx import DateTime

import datetime
####
from openerp import netsvc
import time
####
class mro_order(osv.osv):
    """
    Maintenance Orders
    """
    _name = 'mro.order'
    _description = 'Maintenance Order'
import time

from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
from openerp.tools.translate import _
from openerp import netsvc

class mro_order(osv.osv):
    """
    Assign Technician to Maintenance Orders 
    """
    _name = 'mro.order'
    _description = 'Assign Technician to Maintenance Orders '
    _inherit = 'mro.order'

    _columns = {
        'technician_id':fields.many2one('res.users','Technician',required=True),
    }
    
